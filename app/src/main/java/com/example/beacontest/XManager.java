package com.example.beacontest;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class XManager {

    @NonNull
    private final Context mContext;

    @Nullable
    protected static volatile XManager sInstance = null;
    private static final Object SINGLETON_LOCK = new Object();
    @NonNull
    private static final String TAG = "XManager";
    private static boolean sManifestCheckingDisabled = false;
    private boolean mMainProcess = false;
    private boolean mScheduledScanJobsEnabled = false;

    @Nullable
    private Notification mForegroundServiceNotification = null;
    private int mForegroundServiceNotificationId = -1;
    @NonNull
    public static XManager getInstanceForApplication(@NonNull Context context) {
        XManager instance = sInstance;
        if (instance == null) {
            synchronized (SINGLETON_LOCK) {
                instance = sInstance;
                if (instance == null) {
                    sInstance = instance = new XManager(context);
                }
            }
        }
        return instance;
    }

    protected XManager(@NonNull Context context) {
        mContext = context.getApplicationContext();
        checkIfMainProcess();
        if (!sManifestCheckingDisabled) {
            verifyServiceDeclaration();
        }
        setScheduledScanJobsEnabledDefault();
    }

    protected void checkIfMainProcess() {
        ProcessUtils processUtils = new ProcessUtils(mContext);
        String processName = processUtils.getProcessName();
        String packageName = processUtils.getPackageName();
        int pid = processUtils.getPid();
        mMainProcess = processUtils.isMainProcess();
        Log.i(TAG, "BeaconManager started up on pid "+pid+" named '"+processName+"' for application package '"+packageName+"'.  isMainProcess="+mMainProcess);
    }


    private void verifyServiceDeclaration() {
        final PackageManager packageManager = mContext.getPackageManager();
        final Intent intent = new Intent(mContext, XService.class);
        List<ResolveInfo> resolveInfo =
                packageManager.queryIntentServices(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        if (resolveInfo != null && resolveInfo.isEmpty()) {
            throw new ServiceNotDeclaredException();
        }
    }

    public class ServiceNotDeclaredException extends RuntimeException {
        public ServiceNotDeclaredException() {
            super("The BeaconService is not properly declared in AndroidManifest.xml.  If using Eclipse," +
                    " please verify that your project.properties has manifestmerger.enabled=true");
        }
    }

    public boolean isMainProcess() {
        return mMainProcess;
    }

    private void setScheduledScanJobsEnabledDefault() {
        mScheduledScanJobsEnabled = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    /**
     * @see #enableForegroundServiceScanning
     * @return The notification shown for the beacon scanning service, if so configured
     */
    public Notification getForegroundServiceNotification() {
        return mForegroundServiceNotification;
    }


    /**
     * @see #enableForegroundServiceScanning
     * @return The notification shown for the beacon scanning service, if so configured
     */
    public int getForegroundServiceNotificationId() {
        return mForegroundServiceNotificationId;
    }

    public void enableForegroundServiceScanning(Notification notification, int notificationId)
            throws IllegalStateException {
        if (isAnyConsumerBound()) {
            throw new IllegalStateException("May not be called after consumers are already bound.");
        }
        if (notification == null) {
            throw new NullPointerException("Notification cannot be null");
        }
        setEnableScheduledScanJobs(false);
        mForegroundServiceNotification = notification;
        mForegroundServiceNotificationId = notificationId;
    }

    /**
     * Configures using a `ScanJob` run with the `JobScheduler` to perform scans rather than using a
     * long-running `BeaconService` to do so.
     *
     * Calling with true on devices older than Android L (5.0) will not apply the change
     * as the JobScheduler is not available.
     *
     * This value defaults to true on Android O+ and false on devices with older OS versions.
     * Accepting the default value of false is recommended on Android N and earlier because
     * otherwise beacon scans may be run only once every 15 minutes in the background, and no low
     * power scans may be performed between scanning cycles.
     *
     * Setting this value to false will disable ScanJobs when the app is run on Android 8+, which
     * can prohibit delivery of callbacks when the app is in the background unless the scanning
     * process is running in a foreground service.
     *
     * This method may only be called if bind() has not yet been called, otherwise an
     * `IllegalStateException` is thown.
     *
     * @param enabled
     */

    public void setEnableScheduledScanJobs(boolean enabled) {
//        if (isAnyConsumerBound()) {
//            Log.e(TAG, "ScanJob may not be configured because a consumer is" +
//                    " already bound.");
//            throw new IllegalStateException("Method must be called before calling bind()");
//        }
        if (enabled && android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.e(TAG, "ScanJob may not be configured because JobScheduler is not" +
                    " availble prior to Android 5.0");
            return;
        }
        if (!enabled && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.w(TAG, "Disabling ScanJobs on Android 8+ may disable delivery of "+
                    "beacon callbacks in the background unless a foreground service is active.");
        }
        if(!enabled && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            XJobScheduler.getInstance().cancelSchedule(mContext);
        }
        mScheduledScanJobsEnabled = enabled;
    }

}
