package com.example.beacontest;

import android.app.job.JobScheduler;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import org.jetbrains.annotations.Nullable;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class XJobScheduler {
    private static final String TAG = XJobScheduler.class.getSimpleName();
    private static final Object SINGLETON_LOCK = new Object();
    private static final long MIN_MILLIS_BETWEEN_SCAN_JOB_SCHEDULING = 10000L;
    @Nullable
    private static volatile XJobScheduler sInstance = null;
    @NonNull
    private Long mScanJobScheduleTime = 0L;

    @NonNull
    public static XJobScheduler getInstance() {
        XJobScheduler instance = sInstance;
        if (instance == null) {
            synchronized (SINGLETON_LOCK) {
                instance = sInstance;
                if (instance == null) {
                    sInstance = instance = new XJobScheduler();
                }
            }
        }
        return instance;
    }

    private XJobScheduler() {
    }

    public void cancelSchedule(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

    }
}
