package com.example.beacontest

import org.altbeacon.beacon.Region
import org.altbeacon.beacon.startup.BootstrapNotifier
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.startup.RegionBootstrap
import org.altbeacon.beacon.BeaconParser
import android.R
import android.app.*
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.content.Intent
import android.os.Handler
import java.util.*


class APP : Application(), BootstrapNotifier {
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate() {
        super.onCreate()
        BeaconManager.setDebug(true)
        val beaconManager = BeaconManager.getInstanceForApplication(this)
        
//        setScanPeriodsToDesiredTimes()

//        beaconManager.setAndroidLScanningDisabled(true)

        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"))

        // Calling callPotentiallyBrokenMethods before starting the service will cause duplicate events
//        callPotentiallyBrokenMethods()

        startForegroundService()

        // Calling callPotentiallyBrokenMethods after starting the service will work correctly
        // callPotentiallyBrokenMethods();

        val region = Region("backgroundRegion", null, null, null)
        val regionBootstrap = RegionBootstrap(this, region)

    }

    protected fun startForegroundService() {
        val beaconManager = BeaconManager.getInstanceForApplication(this)
        beaconManager.enableForegroundServiceScanning(getNotificationBuilder(), 123)

        mHandler = Handler()
        mRunnable = Runnable { showRandomNumber() }
        mHandler.postDelayed(mRunnable, 5000)
//        val serviceClass = RandomService::class.java
//
//        // Initialize a new Intent instance
//        val intent = Intent(applicationContext, serviceClass)
//        if (!isServiceRunning(serviceClass)) {
//            // Start the service
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForegroundService(intent)
//            } else {
//                startService(intent)
//            }
//
//        }
    }

    private fun showRandomNumber() {
        val rand = Random()
        val number = rand.nextInt(100)
        toast("Random Number : $number")
        mHandler.postDelayed(mRunnable, 5000)
    }

    private fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        // Loop through the running services
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                // If the service is running then return true
                return true
            }
        }
        return false
    }

    fun getNotificationBuilder(): Notification {
        val NOTIFICATION_CHANNEL_ID = "123"
        val NOTIFICATION_NAME = "aaaaa"

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.mipmap.sym_def_app_icon)
            .setContentTitle("Beacon Reference Application")
            .setContentText("Scanning for Beacons")
            .setAutoCancel(false )
            .setDefaults(Notification.DEFAULT_ALL)
            .setWhen(System.currentTimeMillis())


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_NAME,
                NotificationManager.IMPORTANCE_DEFAULT)
            channel.canBypassDnd()
            notificationManager.createNotificationChannel(channel)
        }
        return notificationBuilder.build()
    }

    override fun didDetermineStateForRegion(p0: Int, p1: Region?) {
        Log.d("vic", "didDetermineStateForRegion")
    }

    override fun didEnterRegion(p0: Region?) {
        Log.d("vic", "didEnterRegion")
    }

    override fun didExitRegion(p0: Region?) {
        Log.d("vic", "didExitRegion")
    }
}