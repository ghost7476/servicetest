package com.example.beacontest

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import java.nio.file.Files.size
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.content.ComponentName



class MainActivity : AppCompatActivity() {
    protected lateinit var frgMgr: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        fixview()
        fixvar()
        fixListener()
    }


    public override fun onResume() {
        super.onResume()
    }

    private fun fixview() {
        frgMgr = supportFragmentManager
    }

    private fun fixvar() {
    }

    private fun fixListener() {
        val dialogPermissionListener = DialogOnDeniedPermissionListener.Builder
            .withContext(this)
            .withTitle("location permission")
            .withMessage("location permission is needed to take ")
            .withButtonText("ok")
            .build()

        Dexter.withActivity(this)
            .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
            .withListener(dialogPermissionListener).check()
    }


    val currentFragment: Fragment?
        get() {
            val frgTag = frgMgr.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1).name
            val currentFrg = frgMgr.findFragmentByTag(frgTag)
            return currentFrg
        }

    fun replaceFragment(fragment: Fragment, animatorResIn: Int, animatorResOut: Int) {
        val fgTag = fragment.javaClass.name
        val fragmentTransaction = frgMgr.beginTransaction()
        fragmentTransaction
            .setCustomAnimations(animatorResIn, animatorResOut, 0, 0)
            .replace(R.id.frg_containt, fragment, fgTag)
            .addToBackStack(fgTag)
            .commit()
    }

    fun addFragment(fragment: Fragment, animatorResIn: Int, animatorResOut: Int) {
        val fgTag = fragment.javaClass.name
        val fragmentTransaction = frgMgr.beginTransaction()
        fragmentTransaction
            .setCustomAnimations(animatorResIn, animatorResOut, 0, 0)
            .add(R.id.frg_containt, fragment, fgTag)
            .addToBackStack(fgTag)
            .commit()
    }


    fun close_slef() {
        frgMgr.popBackStack()
    }
}

fun Context.toast(message:String){
    Toast.makeText(applicationContext,message,Toast.LENGTH_SHORT).show()
}