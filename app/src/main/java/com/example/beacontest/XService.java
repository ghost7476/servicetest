package com.example.beacontest;

import android.app.Notification;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.os.*;
import android.support.annotation.MainThread;
import android.util.Log;
import java.lang.ref.WeakReference;

public class XService  extends Service {

    public static final String TAG = "XService";

    public class BeaconBinder extends Binder {
        public XService getService() {
            Log.i(TAG, "getService of BeaconBinder called");
            // Return this instance of LocalService so clients can call public methods
            return XService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    static class IncomingHandler extends Handler {
        private final WeakReference<XService> mService;

        IncomingHandler(XService service) {
            /*
             * Explicitly state this uses the main thread. Without this we defer to where the
             * service instance is initialized/created; which is usually the main thread anyways.
             * But by being explicit we document our code design expectations for where things run.
             */
            super(Looper.getMainLooper());
            mService = new WeakReference<XService>(service);
        }

        @MainThread
        @Override
        public void handleMessage(Message msg) {
            XService service = mService.get();
            if (service != null) {

            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler(this));

    @MainThread
    @Override
    public void onCreate() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            bluetoothCrashResolver = new BluetoothCrashResolver(this);
//            bluetoothCrashResolver.start();
        }

//        mScanHelper = new ScanHelper(this);
//        if (mScanHelper.getCycledScanner() == null) {
//            mScanHelper.createCycledLeScanner(false, bluetoothCrashResolver);
//        }
//        mScanHelper.setMonitoringStatus(MonitoringStatus.getInstanceForApplication(this));
//        mScanHelper.setRangedRegionState(new HashMap<Region, RangeState>());
//        mScanHelper.setBeaconParsers(new HashSet<BeaconParser>());
//        mScanHelper.setExtraDataBeaconTracker(new ExtraDataBeaconTracker());

        XManager xManager = XManager.getInstanceForApplication(getApplicationContext());
        if (xManager.isMainProcess()) {
            Log.i(TAG, "beaconService version %s is starting up on the main process" + BuildConfig.VERSION_NAME);
            // if we are on the main process, we use local broadcast notifications to deliver results.
//            ensureNotificationProcessorSetup();
        }
        else {
            Log.i(TAG, "beaconService version %s is starting up on a separate process" + BuildConfig.VERSION_NAME);
            ProcessUtils processUtils = new ProcessUtils(this);
            Log.i(TAG, "beaconService PID is "+processUtils.getPid()+" with process name "+processUtils.getProcessName());
        }

        String longScanForcingEnabled = getManifestMetadataValue("longScanForcingEnabled");
        if (longScanForcingEnabled != null && longScanForcingEnabled.equals("true")) {
            Log.i(TAG, "longScanForcingEnabled to keep scans going on Android N for > 30 minutes");
//            if (mScanHelper.getCycledScanner() != null) {
//                mScanHelper.getCycledScanner().setLongScanForcingEnabled(true);
//            }
        }

//        mScanHelper.reloadParsers();
//
//        DistanceCalculator defaultDistanceCalculator =  new ModelSpecificDistanceCalculator(this, XManager.getDistanceModelUpdateUrl());
//        Beacon.setDistanceCalculator(defaultDistanceCalculator);

        this.startForegroundIfConfigured();
    }

    private String getManifestMetadataValue(String key) {
        String value = null;
        try {
            PackageItemInfo info = this.getPackageManager().getServiceInfo(new ComponentName(this, XService.class), PackageManager.GET_META_DATA);
            if (info != null && info.metaData != null) {
                return info.metaData.get(key).toString();
            }
        }
        catch (PackageManager.NameNotFoundException e) {
        }
        return null;
    }

    private void startForegroundIfConfigured() {
        XManager xManager = XManager.getInstanceForApplication(
                this.getApplicationContext());
        Notification notification = xManager
                .getForegroundServiceNotification();
        int notificationId = xManager
                .getForegroundServiceNotificationId();
        if (notification != null &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.startForeground(notificationId, notification);
        }
    }
}
