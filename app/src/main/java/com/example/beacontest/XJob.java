package com.example.beacontest;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class XJob extends JobService {
    private static final String TAG = XJob.class.getSimpleName();
    /*
        Periodic scan jobs are used in general, but they cannot be started immediately.  So we have
        a second immediate scan job to kick off when scanning gets started or settings changed.
        Once the periodic one gets run, the immediate is cancelled.
     */
    private static int sOverrideImmediateScanJobId = -1;
    private static int sOverridePeriodicScanJobId = -1;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        // We start off on the main UI thread here.
        // But the ScanState restore from storage sometimes hangs, so we start new thread here just
        // to kick that off.  This way if the restore hangs, we don't hang the UI thread.
        new Thread(new Runnable() {
            public void run() {
                if (!initialzeScanHelper()) {
                    LogManager.e(TAG, "Cannot allocate a scanner to look for beacons.  System resources are low.");
                    ScanJob.this.jobFinished(jobParameters , false);
                }
                ScanJobScheduler.getInstance().ensureNotificationProcessorSetup(getApplicationContext());
                if (jobParameters.getJobId() == getImmediateScanJobId(ScanJob.this)) {
                    LogManager.i(TAG, "Running immediate scan job: instance is "+this);
                }
                else {
                    LogManager.i(TAG, "Running periodic scan job: instance is "+this);
                }

                List<ScanResult> queuedScanResults = ScanJobScheduler.getInstance().dumpBackgroundScanResultQueue();
                LogManager.d(TAG, "Processing %d queued scan resuilts", queuedScanResults.size());
                for (ScanResult result : queuedScanResults) {
                    ScanRecord scanRecord = result.getScanRecord();
                    if (scanRecord != null) {
                        mScanHelper.processScanResult(result.getDevice(), result.getRssi(), scanRecord.getBytes());
                    }
                }
                LogManager.d(TAG, "Done processing queued scan resuilts");

                boolean startedScan;
                if (mInitialized) {
                    LogManager.d(TAG, "Scanning already started.  Resetting for current parameters");
                    startedScan = restartScanning();
                }
                else {
                    startedScan = startScanning();
                }
                mStopHandler.removeCallbacksAndMessages(null);

                if (startedScan) {
                    LogManager.i(TAG, "Scan job running for "+mScanState.getScanJobRuntimeMillis()+" millis");
                    mStopHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            LogManager.i(TAG, "Scan job runtime expired: " + ScanJob.this);
                            stopScanning();
                            mScanState.save();
                            ScanJob.this.jobFinished(jobParameters , false);

                            // need to execute this after the current block or Android stops this job prematurely
                            mStopHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    scheduleNextScan();
                                }
                            });

                        }
                    }, mScanState.getScanJobRuntimeMillis());
                }
                else {
                    Log.i(TAG, "Scanning not started so Scan job is complete.");
                    ScanJob.this.jobFinished(jobParameters , false);
                }
            }
        }).start();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
